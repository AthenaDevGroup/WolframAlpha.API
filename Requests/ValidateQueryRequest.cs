﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using WolframAlpha.API.Components;

namespace WolframAlpha.API.Requests
{
    /// <summary>
    ///     Class for making validate requests to the WolframAlpha API and parsing the result.
    /// </summary>
    public class ValidateQueryRequest : ApiRequest<QueryResult>
    {
    }
}