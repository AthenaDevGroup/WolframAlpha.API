﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

using WolframAlpha.API.Components;

namespace WolframAlpha.API.Requests
{
    [ContractClassFor(typeof(IApiRequest<>))]
    internal abstract class ApiRequestContract<T> : IApiRequest<T> where T : ApiResult
    {
        public Task<T> ExecuteAsync(Uri requestUri)
        {
            Contract.Requires(requestUri != null);

            return default(Task<T>);
        }

        public Task<T> ExecuteAsync(Uri requestUri, CancellationToken cancellationToken)
        {
            Contract.Requires(requestUri != null);

            return default(Task<T>);
        }
    }
}