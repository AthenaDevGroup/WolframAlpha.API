﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

using WolframAlpha.API.Components;

namespace WolframAlpha.API.Requests
{
    /// <summary>
    ///     Interface for an api request.
    /// </summary>
    [ContractClass(typeof(ApiRequestContract<>))]
    public interface IApiRequest<T> where T : ApiResult
    {
        /// <summary>
        ///     Execute the request.
        /// </summary>
        /// <param name="requestUri"></param>
        /// <returns></returns>
        Task<T> ExecuteAsync(Uri requestUri);

        /// <summary>
        ///     Execute the request.
        /// </summary>
        /// <param name="requestUri"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<T> ExecuteAsync(Uri requestUri, CancellationToken cancellationToken);
    }
}