﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

namespace WolframAlpha.API
{
    /// <summary>
    ///     Constants.
    /// </summary>
    public static class ApiConstants
    {
        /// <summary>
        ///     Base URL for a query.
        /// </summary>
        public const string QueryBaseUrl = @"http://api.wolframalpha.com/v2/query";

        /// <summary>
        ///     Base URL for a validate query.
        /// </summary>
        public const string ValidateQueryBaseUrl = @"http://api.wolframalpha.com/v2/validatequery";
    }
}