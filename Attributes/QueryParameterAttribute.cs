﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Diagnostics;

namespace WolframAlpha.API.Attributes
{
    /// <summary>
    ///     Query Parameter Attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property), DebuggerDisplay("{Name} Required? {Required}")]
    public class QueryParameterAttribute : Attribute
    {
        /// <summary>
        ///     Create a new QueryParameterAttribute.
        /// </summary>
        /// <param name="name"></param>
        public QueryParameterAttribute(string name) : this(name, false)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Name is required.", nameof(name));
        }

        /// <summary>
        ///     Create a new QueryParameterAttribute.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="required"></param>
        public QueryParameterAttribute(string name, bool required)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Name is required.", nameof(name));

            Name = name;
            Required = required;
        }

        /// <summary>
        ///     Name of the query parameter.
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Is this parameter required?
        /// </summary>
        public bool Required { get; }
    }
}