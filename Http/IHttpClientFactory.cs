﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Net.Http;

namespace WolframAlpha.API.Http
{
    /// <summary>
    ///     Interface for an HttpClientFactory
    /// </summary>
    public interface IHttpClientFactory
    {
        /// <summary>
        ///     Create a new instance of an HttpClient.
        /// </summary>
        /// <returns></returns>
        HttpClient CreateClient();

        /// <summary>
        ///     Create a new instance of an HttpRequestMessage.
        /// </summary>
        /// <param name="httpMethod"></param>
        /// <param name="requestUri"></param>
        /// <returns></returns>
        HttpRequestMessage CreateRequestMessage(HttpMethod httpMethod, Uri requestUri);
    }
}