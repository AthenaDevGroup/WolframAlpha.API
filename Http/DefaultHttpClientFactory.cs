﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System;
using System.Net.Http;

namespace WolframAlpha.API.Http
{
    /// <summary>
    ///     Default implementation of the IHttpClientFactory interface.
    /// </summary>
    internal class DefaultHttpClientFactory : IHttpClientFactory
    {
        /// <summary>
        ///     Create a new instance of an HttpClient.
        /// </summary>
        /// <returns></returns>
        public HttpClient CreateClient() => new HttpClient();

        /// <summary>
        ///     Create a new instance of an HttpRequestMessage.
        /// </summary>
        /// <param name="httpMethod"></param>
        /// <param name="requestUri"></param>
        /// <returns></returns>
        public HttpRequestMessage CreateRequestMessage(HttpMethod httpMethod, Uri requestUri) => new HttpRequestMessage(
            httpMethod, requestUri);
    }
}