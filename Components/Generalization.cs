﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     For some types of queries, Wolfram|Alpha decides that although it can provide some results for the precise query
    ///     that
    ///     was given,
    ///     there is a "generalization" of the query for which more information can be provided.
    /// </summary>
    [XmlRoot("generalization"), DebuggerDisplay("{" + nameof(Url) + ",nq}")]
    public class Generalization
    {
        /// <summary>
        ///     Topic.
        /// </summary>
        [XmlAttribute("topic")]
        public string Topic { get; set; }

        /// <summary>
        ///     Desc.
        /// </summary>
        [XmlAttribute("desc")]
        public string Desc { get; set; }

        /// <summary>
        ///     Url.
        /// </summary>
        [XmlAttribute("url")]
        public string Url { get; set; }
    }
}