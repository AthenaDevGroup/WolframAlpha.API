﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     AssumptionValue.
    /// </summary>
    [XmlRoot("value"), DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    public class AssumptionValue
    {
        /// <summary>
        ///     Name.
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        ///     Desc.
        /// </summary>
        [XmlAttribute("desc")]
        public string Desc { get; set; }

        /// <summary>
        ///     Input.
        /// </summary>
        [XmlAttribute("input")]
        public string Input { get; set; }
    }
}