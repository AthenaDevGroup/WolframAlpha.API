﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     If you enter "chicag" as a query, Wolfram|Alpha assumes you meant "chicago."
    /// </summary>
    [XmlRoot("spellcheck"), DebuggerDisplay("{" + nameof(Word) + ",nq}")]
    public class SpellCheckWarning
    {
        /// <summary>
        ///     Word.
        /// </summary>
        [XmlAttribute("word")]
        public string Word { get; set; }

        /// <summary>
        ///     Suggestion.
        /// </summary>
        [XmlAttribute("suggestion")]
        public string Suggestion { get; set; }

        /// <summary>
        ///     Text.
        /// </summary>
        [XmlAttribute("text")]
        public string Text { get; set; }
    }
}