﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Each tip gives a line of text that you might choose to display to users.
    /// </summary>
    [XmlRoot("tips"), DebuggerDisplay("Count: {" + nameof(Count) + ",nq}")]
    public class TipsCollection
    {
        /// <summary>
        ///     Count.
        /// </summary>
        [XmlAttribute("count")]
        public string Count { get; set; }

        /// <summary>
        ///     Tips.
        /// </summary>
        [XmlElement("tip")]
        public Tip[] Tips { get; set; }

        /// <summary>
        ///     Get Tip by index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <returns>Tip</returns>
        [XmlIgnore]
        public Tip this[int i]
        {
            get
            {
                Contract.Requires(0 <= i);
                Contract.Requires(i < Tips.Length);

                return Tips[i];
            }
        }
    }
}