﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Tip.
    /// </summary>
    [XmlRoot("tip"), DebuggerDisplay("{" + nameof(Text) + ",nq}")]
    public class Tip
    {
        /// <summary>
        ///     Text.
        /// </summary>
        [XmlAttribute("text")]
        public string Text { get; set; }
    }
}