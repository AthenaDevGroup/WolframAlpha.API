﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     The warnings element occurs as a subelement of queryresult.
    ///     It contains warning subelements, each of which describes a particular warning generated during the query.
    /// </summary>
    [XmlRoot("warnings"), DebuggerDisplay("Count: {" + nameof(Count) + ",nq}")]
    public class WarningCollection
    {
        /// <summary>
        ///     Count.
        /// </summary>
        [XmlAttribute("count")]
        public string Count { get; set; }

        /// <summary>
        ///     SpellChecks.
        /// </summary>
        [XmlElement("spellcheck")]
        public SpellCheckWarning[] SpellChecks { get; set; }

        /// <summary>
        ///     Delimiters.
        /// </summary>
        [XmlElement("delimiters")]
        public DelimitersWarning[] Delimiters { get; set; }

        /// <summary>
        ///     Translations.
        /// </summary>
        [XmlElement("translation")]
        public TranslationWarning[] Translations { get; set; }

        /// <summary>
        ///     Reinterprets.
        /// </summary>
        [XmlElement("reinterpret")]
        public ReinterpretWarning[] Reinterprets { get; set; }
    }
}