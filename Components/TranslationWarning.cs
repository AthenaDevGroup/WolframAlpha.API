﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Wolfram|Alpha will translate some queries from non-English languages into English.
    ///     At present, the only way to see the translation warning is to turn on automatic translation with the
    ///     translation=true
    ///     URL parameter.
    /// </summary>
    [XmlRoot("translation"), DebuggerDisplay("{" + nameof(Phrase) + ",nq}")]
    public class TranslationWarning
    {
        /// <summary>
        ///     Phrase.
        /// </summary>
        [XmlAttribute("phrase")]
        public string Phrase { get; set; }

        /// <summary>
        ///     Trans.
        /// </summary>
        [XmlAttribute("trans")]
        public string Trans { get; set; }

        /// <summary>
        ///     Lang.
        /// </summary>
        [XmlAttribute("lang")]
        public string Lang { get; set; }

        /// <summary>
        ///     Text.
        /// </summary>
        [XmlAttribute("text")]
        public string Text { get; set; }
    }
}