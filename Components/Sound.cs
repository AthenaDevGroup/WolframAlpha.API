﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Sound.
    /// </summary>
    [XmlRoot("sound"), DebuggerDisplay("{" + nameof(Url) + ",nq}")]
    public class Sound
    {
        /// <summary>
        ///     Url.
        /// </summary>
        [XmlAttribute("url")]
        public string Url { get; set; }

        /// <summary>
        ///     Type.
        /// </summary>
        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}