﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     If Wolfram|Alpha cannot understand your query, but recognizes it as a foreign language, it will generate a
    ///     languagemsg element.
    /// </summary>
    [XmlRoot("languagemsg"), DebuggerDisplay("{" + nameof(English) + ",nq}")]
    public class LanguageMsg
    {
        /// <summary>
        ///     Textual message in English.
        /// </summary>
        [XmlAttribute("english")]
        public string English { get; set; }

        /// <summary>
        ///     Textual message in whatever language the query appears to be.
        /// </summary>
        [XmlAttribute("other")]
        public string Other { get; set; }
    }
}