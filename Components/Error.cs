﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     The error element occurs as either a subelement of queryresult, if there was a failure that prevented any result
    ///     from
    ///     being returned,
    ///     or as a subelement of pod, if there was an error that only prevented the result from a given pod from being
    ///     returned.
    /// </summary>
    [XmlRoot("error"), DebuggerDisplay("{" + nameof(Code) + ",nq}")]
    public class Error
    {
        /// <summary>
        ///     The error code, an integer.
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        ///     A short message describing the error.
        /// </summary>
        [XmlElement("msg")]
        public string Msg { get; set; }
    }
}