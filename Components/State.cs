﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     State.
    /// </summary>
    [XmlRoot("state"), DebuggerDisplay("{" + nameof(Name) + ",nq}")]
    public class State
    {
        /// <summary>
        ///     Name.
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        ///     Input.
        /// </summary>
        [XmlAttribute("input")]
        public string Input { get; set; }
    }
}