﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     If you enter a query with mismatched delimiters like "sin(x", Wolfram|Alpha attempts to fix the problem and reports
    ///     this as a warning.
    /// </summary>
    [XmlRoot("delimiters"), DebuggerDisplay("{" + nameof(Text) + ",nq}")]
    public class DelimitersWarning
    {
        /// <summary>
        ///     Text.
        /// </summary>
        [XmlAttribute("text")]
        public string Text { get; set; }
    }
}