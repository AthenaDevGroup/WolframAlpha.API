﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Some queries generate sounds as part of their output.
    /// </summary>
    [XmlRoot("sounds"), DebuggerDisplay("Count: {" + nameof(Count) + ",nq}")]
    public class SoundCollection
    {
        /// <summary>
        ///     Count.
        /// </summary>
        [XmlAttribute("count")]
        public string Count { get; set; }

        /// <summary>
        ///     Sounds.
        /// </summary>
        [XmlElement("sound")]
        public Sound[] Sounds { get; set; }

        /// <summary>
        ///     Get Sound by index.
        /// </summary>
        /// <param name="i">Inde</param>
        /// <returns>Sound</returns>
        [XmlIgnore]
        public Sound this[int i]
        {
            get
            {
                Contract.Requires(0 <= i);
                Contract.Requires(i < Sounds.Length);

                return Sounds[i];
            }
        }
    }
}