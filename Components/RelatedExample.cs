﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     RelatedExample.
    /// </summary>
    [XmlRoot("relatedexample"), DebuggerDisplay("{" + nameof(Input) + ",nq}")]
    public class RelatedExample
    {
        /// <summary>
        ///     Input.
        /// </summary>
        [XmlAttribute("input")]
        public string Input { get; set; }

        /// <summary>
        ///     Desc.
        /// </summary>
        [XmlAttribute("desc")]
        public string Desc { get; set; }

        /// <summary>
        ///     Category.
        /// </summary>
        [XmlAttribute("category")]
        public string Category { get; set; }

        /// <summary>
        ///     CategoryThumb.
        /// </summary>
        [XmlAttribute("categorythumb")]
        public string CategoryThumb { get; set; }

        /// <summary>
        ///     CategoryPage.
        /// </summary>
        [XmlAttribute("categorypage")]
        public string CategoryPage { get; set; }
    }
}