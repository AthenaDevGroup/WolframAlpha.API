﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Some pods on the Wolfram|Alpha website have text buttons in their lower-right corners that provide extra
    ///     information
    ///     about the contents of that pod.
    ///     The data for these "information" links is available in the API via the infos element, which appears inside any pod
    ///     elements for which information links are available.
    /// </summary>
    [XmlRoot("infos"), DebuggerDisplay("Count: {" + nameof(Count) + ",nq}")]
    public class InfoCollection
    {
        /// <summary>
        ///     Count.
        /// </summary>
        [XmlAttribute("count")]
        public string Count { get; set; }

        /// <summary>
        ///     Infos.
        /// </summary>
        [XmlElement("info")]
        public Info[] Infos { get; set; }

        /// <summary>
        ///     Get Info by index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <returns>Info</returns>
        [XmlIgnore]
        public Info this[int i]
        {
            get
            {
                Contract.Requires(0 <= i);
                Contract.Requires(i < Infos.Length);

                return Infos[i];
            }
        }
    }
}