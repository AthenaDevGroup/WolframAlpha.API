﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Unit.
    /// </summary>
    [XmlRoot("unit"), DebuggerDisplay("{" + nameof(Short) + ",nq}")]
    public class Unit
    {
        /// <summary>
        ///     Short name.
        /// </summary>
        [XmlAttribute("short")]
        public string Short { get; set; }

        /// <summary>
        ///     Long name.
        /// </summary>
        [XmlAttribute("long")]
        public string Long { get; set; }
    }
}