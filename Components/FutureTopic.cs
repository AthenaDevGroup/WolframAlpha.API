﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Queries that refer to topics that are under development generate a futuretopic element.
    /// </summary>
    [XmlRoot("futuretopic"), DebuggerDisplay("{" + nameof(Topic) + ",nq}")]
    public class FutureTopic
    {
        /// <summary>
        ///     Topic.
        /// </summary>
        [XmlAttribute("topic")]
        public string Topic { get; set; }

        /// <summary>
        ///     Msg.
        /// </summary>
        [XmlAttribute("msg")]
        public string Msg { get; set; }
    }
}