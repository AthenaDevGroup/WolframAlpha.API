﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Some states are logically grouped into sets.
    /// </summary>
    [XmlRoot("statelist"), DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
    public class StateList
    {
        /// <summary>
        ///     Count.
        /// </summary>
        [XmlAttribute("count")]
        public string Count { get; set; }

        /// <summary>
        ///     The value attribute of the statelist element names the state that is currently in effect.
        /// </summary>
        [XmlAttribute("value")]
        public string Value { get; set; }

        /// <summary>
        ///     States.
        /// </summary>
        [XmlElement("state")]
        public State[] States { get; set; }

        /// <summary>
        ///     Get State by index.
        /// </summary>
        /// <param name="i">Index</param>
        /// <returns>State.</returns>
        [XmlIgnore]
        public State this[int i]
        {
            get
            {
                Contract.Requires(0 <= i);
                Contract.Requires(i < States.Length);

                return States[i];
            }
        }

        private string DebuggerDisplay => string.Format("Count: {0} Value: {1}", Count, Value);
    }
}