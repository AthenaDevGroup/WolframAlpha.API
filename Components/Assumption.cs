﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     The assumption element is a subelement of assumptions.
    ///     It defines a single assumption, typically about the meaning of a word or phrase, and a series of possible other
    ///     values.
    /// </summary>
    [XmlRoot("assumption"), DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
    public class Assumption
    {
        /// <summary>
        ///     Type.
        /// </summary>
        [XmlAttribute("type")]
        public string Type { get; set; }

        /// <summary>
        ///     Word.
        /// </summary>
        [XmlAttribute("word")]
        public string Word { get; set; }

        /// <summary>
        ///     Count.
        /// </summary>
        [XmlAttribute("count")]
        public string Count { get; set; }

        /// <summary>
        ///     Values.
        /// </summary>
        [XmlElement("value")]
        public AssumptionValue[] Values { get; set; }

        private string DebuggerDisplay => string.Format("{0} {1}", Type, Word);
    }
}