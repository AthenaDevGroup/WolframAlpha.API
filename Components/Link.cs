﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Url link.
    /// </summary>
    [XmlRoot("link"), DebuggerDisplay("{" + nameof(Url) + ",nq}")]
    public class Link
    {
        /// <summary>
        ///     Url.
        /// </summary>
        [XmlAttribute("url")]
        public string Url { get; set; }

        /// <summary>
        ///     Text.
        /// </summary>
        [XmlAttribute("text")]
        public string Text { get; set; }

        /// <summary>
        ///     Title.
        /// </summary>
        [XmlAttribute("title")]
        public string Title { get; set; }
    }
}