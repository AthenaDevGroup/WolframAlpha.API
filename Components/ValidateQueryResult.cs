﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     queryresult is the outer wrapper for all results from the query function.
    /// </summary>
    [XmlRoot("queryresult")]
    public class ValidateQueryResult : ApiResult
    {
    }
}