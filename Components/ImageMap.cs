﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.Diagnostics;
using System.Xml.Serialization;

namespace WolframAlpha.API.Components
{
    /// <summary>
    ///     Many pods on the Wolfram|Alpha website have HTML image maps associated with them, so that you can click parts of
    ///     the
    ///     pod image to execute queries.
    ///     Most table-style pods have this property, so that each element in the table or list can be clicked to trigger a
    ///     query
    ///     based on the content of that item.
    /// </summary>
    [XmlRoot("imagemap"), DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
    public class ImageMap
    {
        /// <summary>
        ///     Rects.
        /// </summary>
        [XmlElement("rect")]
        public Rect[] Rects { get; set; }

        private string DebuggerDisplay => string.Format("Count: {0}", Rects != null ? Rects.Length : 0);
    }
}