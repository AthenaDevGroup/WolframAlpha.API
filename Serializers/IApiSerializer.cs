﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.IO;

using WolframAlpha.API.Components;

namespace WolframAlpha.API.Serializers
{
    /// <summary>
    ///     Interface for a serializer.
    /// </summary>
    public interface IApiSerializer<TResult> where TResult : ApiResult
    {
        /// <summary>
        ///     Deserialize directly into an ApiResult.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        TResult Deserialize(string s);

        /// <summary>
        ///     Deserialize directly into an ApiResult.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        TResult Deserialize(Stream stream);
    }
}