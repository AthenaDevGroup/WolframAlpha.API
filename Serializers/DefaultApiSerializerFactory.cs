﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using WolframAlpha.API.Components;

namespace WolframAlpha.API.Serializers
{
    /// <summary>
    ///     Default implementation of the IApiSerializerFactory interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DefaultApiSerializerFactory<T> : IApiSerializerFactory<T> where T : ApiResult
    {
        /// <summary>
        ///     Create a new instance of an ApiSerializer.
        /// </summary>
        /// <returns></returns>
        public IApiSerializer<T> CreateApiSerializer() => new ApiSerializer<T>();
    }
}