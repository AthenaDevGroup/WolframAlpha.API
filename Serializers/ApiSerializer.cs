﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using System.IO;
using System.Xml.Serialization;

using WolframAlpha.API.Components;

namespace WolframAlpha.API.Serializers
{
    /// <summary>
    ///     XmlSerializer for the WolframAlpha api.
    /// </summary>
    /// <typeparam name="TResult">Subclass of ApiResult</typeparam>
    public class ApiSerializer<TResult> : IApiSerializer<TResult> where TResult : ApiResult
    {
        private readonly XmlSerializer _serializer;

        /// <summary>
        ///     Create a new ApiSerializer instance.
        /// </summary>
        public ApiSerializer() => _serializer = new XmlSerializer(typeof(TResult));

        /// <summary>
        ///     Deserialize directly into an ApiResult.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public TResult Deserialize(string s)
        {
            if (string.IsNullOrEmpty(s))
                return default(TResult);

            using (var r = new StringReader(s))
            {
                return (TResult) _serializer.Deserialize(r);
            }
        }

        /// <summary>
        ///     Deserialize directly into an ApiResult.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public TResult Deserialize(Stream stream) => (TResult) _serializer.Deserialize(stream);
    }
}