﻿// ---------------------------------------------------------
// Copyrights (c) 2014-2017 Seditio 🍂 All rights reserved.
// ---------------------------------------------------------

using WolframAlpha.API.Components;

namespace WolframAlpha.API.Serializers
{
    /// <summary>
    ///     Interface for an ApiSerializerFactory.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IApiSerializerFactory<T> where T : ApiResult
    {
        /// <summary>
        ///     Create a new instance of an ApiSerializer.
        /// </summary>
        /// <returns></returns>
        IApiSerializer<T> CreateApiSerializer();
    }
}